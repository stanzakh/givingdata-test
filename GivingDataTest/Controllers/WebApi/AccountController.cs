﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using GivingDataTest.Domain;
using GivingDataTest.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;

namespace GivingDataTest.Controllers.WebApi
{
    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {
        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // POST api/account/logout
        [Route("logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut();
            return Ok();
        }

        // POST api/account/register
        [Route("register")]
        [HttpPost]
        public async Task<IHttpActionResult> Register([FromBody]RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new IdentityUser { UserName = model.Username };

            using (var context = new ApplicationDbContext())
            using (var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(context)))
            {
                var result = await userManager.CreateAsync(user, model.Password);
                return !result.Succeeded ? GetErrorResult(result) : Ok();
            }
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        #endregion
    }
}