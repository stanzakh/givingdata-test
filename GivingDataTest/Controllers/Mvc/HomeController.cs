﻿using System.Web.Mvc;

namespace GivingDataTest.Controllers.Mvc
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}