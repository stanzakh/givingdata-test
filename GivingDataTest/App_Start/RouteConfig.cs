﻿using System.Web.Mvc;
using System.Web.Routing;

namespace GivingDataTest
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("api/v1/{*pathInfo}");
            routes.MapRoute("UseAngularRouting", "{*url}", new { controller = "Home", action = "Index" });
        }
    }
}