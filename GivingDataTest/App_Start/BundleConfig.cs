﻿using System.Web.Optimization;

namespace GivingDataTest
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterScriptBundle(bundles, "~/bundles/app", "~/Scripts/app");
            RegisterStyleBundle(bundles, "~/Content/Styles/css", "~/Content/Styles");
            RegisterVendorBundles(bundles);
        }

        private static void RegisterScriptBundle(BundleCollection bundles, string bundleName, string bundlePath)
        {
            bundles.Add(new ScriptBundle(bundleName).IncludeDirectory(bundlePath, "*.js", true));
        }

        private static void RegisterStyleBundle(BundleCollection bundles, string bundleName, string bundlePath)
        {
            bundles.Add(new StyleBundle(bundleName).IncludeDirectory(bundlePath, "*.css", true));
        }

        private static void RegisterVendorBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/vendor")
                .Include("~/Scripts/vendor/angular.js")
                .Include("~/Scripts/vendor/angular-route.js")
                .Include("~/Scripts/vendor/angular-local-storage.js")
                .Include("~/Scripts/vendor/ui-bootstrap.js"));
        }
    }
}
