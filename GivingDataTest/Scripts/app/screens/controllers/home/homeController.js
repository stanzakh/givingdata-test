﻿angular.module("app").controller("homeController", [
    "$scope", "$location", "$routeParams", "$route", "googleBooksService", "currentUser",
    function ($scope, $location, $routeParams, $route, googleBooksService, currentUser) {

        var getCurrentIndex = function() {
            return $routeParams.page ? Math.max($routeParams.page, 1) - 1 : 0;
        };

        var getCurrentPage = function() {
            return getCurrentIndex() + 1;
        };

        var searchForBooks = function(term, page) {
            googleBooksService.getVolumes($routeParams.term, page).then(function(result) {
                var lowerBound = (page - 1) * 10 + 1;
                var upperBound = (page - 1) * 10 + 10;
                $scope.positionText = "You are viewing results " + lowerBound + "-" + upperBound + " of the " + result.totalItems + " that match your search criteria.";
                $scope.books = result.items;
            });
        };        

        $scope.findBooks = function (term) {
            $location.path("/search").search({ term: term, page: 1 });
        };

        $scope.nextPage = function (term) {
            $location.search({ term: term, page: getCurrentPage() + 1 });
        };

        $scope.previousPage = function (term) {
            $location.search({ term: term, page: getCurrentPage() - 1 });
        };

        var currentPage = getCurrentPage();
        $scope.searchTerm = $routeParams.term;
        $scope.canDisplayPreviousPage = currentPage > 1;
        $scope.currentUser = currentUser.getUserData();
        if ($scope.searchTerm) {
            searchForBooks($scope.searchTerm, currentPage);
        }
    }
]);