﻿angular.module("app").controller("authController", [
    "$scope", "$location", "authService",
    function ($scope, $location, authService) {
        $scope.login = function(username, password) {
            authService.login(username, password).then(function() {
                $location.path("/");
            }).catch(function(error) {
                $scope.authError = error;
            });
        };

        $scope.register = function(username, password, confirmPassword) {
            authService.register(username, password, confirmPassword).then(function() {
                $location.path("/");
            }).catch(function(error) {
                $scope.authError = error;
            });
        };
    }
]);