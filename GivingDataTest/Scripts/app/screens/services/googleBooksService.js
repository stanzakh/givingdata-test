﻿angular.module("app").service("googleBooksService",
    [
        "$http", "$q", function ($http, $q) {

            var endpoint = "https://www.googleapis.com/books/v1/";

            this.getVolumes = function(searchTerm, startIndex) {
                return $http.get(endpoint + "volumes?q=" + searchTerm + "&startIndex=" + startIndex).then(function(result) {
                    return result.data;
                }).catch(function(result) {
                    console.log(result);
                });
            };
        }
    ]);