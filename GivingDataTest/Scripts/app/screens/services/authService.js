﻿angular.module("app").service("authService",
    ["$q", "$http", "$httpParamSerializerJQLike", "currentUser", function ($q, $http, $httpParamSerializerJQLike, currentUser) {

        var login = function(username, password) {
            var data = {
                "grant_type": "password",
                "username": username,
                "password": password
            };

            return $http({
                url: "/token",
                method: "POST",
                data: $httpParamSerializerJQLike(data),
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(function (result) {
                currentUser.setUserData(result.data.userName, result.data["access_token"]);
                console.log(result);
                return $q.resolve();
            }).catch(function (result) {
                currentUser.clearUserData();
                console.log(result);
                return $q.reject(result.data["error_description"]);
            });
        };

        var register = function(username, password, confirmPassword) {
            var data = {
                "username": username,
                "password": password,
                "confirmPassword": confirmPassword
            };

            return $http({
                url: "/api/account/register",
                method: "POST",
                data: $httpParamSerializerJQLike(data),
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(function () {
                return login(username, password);
            }).catch(function (result) {
                currentUser.clearUserData();
                console.log(result);
                return $q.reject(result.data.Message);
            });
        };

        var logout = function() {
            return $http.post("api/account/logout").then(function () {
                currentUser.clearUserData();
                return $q.resolve();
            }).catch(function (result) {
                currentUser.clearUserData();
                console.log(result);
                return $q.reject(result.data["error_description"]);
            });
        }

        this.login = login;
        this.register = register;
        this.logout = logout;
    }]
);