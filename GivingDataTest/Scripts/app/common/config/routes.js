﻿angular.module("app").config([
    "$routeProvider", "$locationProvider", ($routeProvider, $locationProvider) => {
        $routeProvider
            .when("/", {
                templateUrl: constants.templates + "home/index.html",
                controller: "homeController"
            })
            .when("/search", {
                templateUrl: constants.templates + "home/index.html",
                controller: "homeController"
            })
            .when("/login", {
                templateUrl: constants.templates + "auth/login.html",
                controller: "authController"
            })
            .when("/register", {
                templateUrl: constants.templates + "auth/register.html",
                controller: "authController"
            });

        $locationProvider.html5Mode(true);
    }
]);