﻿angular.module("app").controller("navbarController", [
    "$scope", "$location", "currentUser", "authService",
    function ($scope, $location, currentUser, authService) {
        $scope.currentUser = currentUser.getUserData();

        $scope.logout = function () {
            authService.logout().then(function () {
                $location.path("/");
            }).catch(function () {
                console.log("An error has occurred while logging out.");
            });
        };
    }
]);