﻿angular.module("app").factory("BearerAuthInterceptor", ["$window", "$q", "currentUser", function ($window, $q, currentUser) {
    return {
        request: function (config) {
            config.headers = config.headers || {};

            var pathTester = new RegExp("^(?:[a-z]+:)?//", "i");
            var userData = currentUser.getUserData();
            if (userData.isAuthenticated && userData.bearerToken && !pathTester.test(config.url)) {
                config.headers.Authorization = "Bearer " + userData.bearerToken;
            }

            return config || $q.when(config);
        },
        response: function (response) {
            return response || $q.when(response);
        }
    };
}]);

// Register the previously created AuthInterceptor.
angular.module("app").config(["$httpProvider", function ($httpProvider) {
    $httpProvider.interceptors.push("BearerAuthInterceptor");
}]);