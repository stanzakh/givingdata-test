﻿angular.module("app").service("currentUser",
    ["localStorageService", function (localStorageService) {

        var userData = {
            isAuthenticated: localStorageService.get("isAuthenticated"),
            username: localStorageService.get("username"),
            bearerToken: localStorageService.get("bearerToken")
        };

        this.getUserData = function() {
            return userData;
        };

        this.setUserData = function(username, bearerToken) {
            userData.isAuthenticated = true;
            userData.username = username;
            userData.bearerToken = bearerToken;

            localStorageService.set("isAuthenticated", true);
            localStorageService.set("username", username);
            localStorageService.set("bearerToken", bearerToken);
        };

        this.clearUserData = function() {
            userData.isAuthenticated = false;
            userData.username = "";
            userData.bearerToken = "";

            localStorageService.set("isAuthenticated", null);
            localStorageService.set("username", null);
            localStorageService.set("bearerToken", null);
        };
    }]
);