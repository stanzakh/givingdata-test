﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace GivingDataTest.Domain
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }
    }
}