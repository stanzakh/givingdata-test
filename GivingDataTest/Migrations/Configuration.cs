using System.Data.Entity;
using System.Data.Entity.Migrations;
using GivingDataTest.Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace GivingDataTest.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            AddUser(context);
        }

        private bool AddUser(DbContext context)
        {
            var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(context));
            var user = new IdentityUser() {UserName = "admin"};

            if (userManager.FindByName(user.UserName) != null)
            {
                return true;
            }

            var identityResult = userManager.Create(user, "password");
            return identityResult.Succeeded;
        }
    }
}